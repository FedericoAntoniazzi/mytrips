![My trips logo](images/app_icon.png "My Trips logo")
# MyTrips

[[_TOC_]]

## Abstract

This app allows you to save information about your trips such as place, departure date and return date. 

> The app is still in development so a lot of features (such as photos, sharing, departure time, aircraft/bus ticket info...) will be added in future 

## General Technical data

### Screens
This app has:
- *LoginScreen* to perform the login
- *RegisterScreen* to perform registration of a new user
- *HomePage* to view all the trips
- *AddNewTrip* to add a new trip
    
### Database interaction

This project uses Firebase because is really simple to implement and it offers a lot of cool features.

#### Authentication

Firebase allows to implement authentication with lots of services like username and password, google, facebook, github and more but only the firsts two have been implemented.

[Official documentation](https://firebase.google.com/docs/auth)

#### Database

Firebase offers Firestore that is a NoSQL DBMS so there aren't SQL queries and tables because is structure is like a tree.

[Official documentation](https://firebase.google.com/docs/firestore)

## Key Features

Features that have been implemented:
- [x] Login with username and password
- [x] Login with Google
- [x] Registration of a new user using username and password
- [x] Add new trip
- [x] Delete trip from list
- [x] Show all user's trips in a list in the homepage
- [x] Themes (Light, dark and amoled dark)

Planned features:
- [ ] Improve homepage UI
- [ ] Improve addNewTrip screen UI
- [ ] Use a photo instead the blue circle in the homepage when showing trips
- [ ] Trip detail page

## App structure overview
### Wireframe
[Graphical wireframe link](https://drive.google.com/file/d/1H27_7nIzw7irYOpHsvsN6siLPDoesnDu/view?usp=sharing)

```mermaid 
graph TD;
    isUserLoggedIn? --> Homepage;
    isUserLoggedIn? --> LoginPage;
    LoginPage --> RegisterPage;
    LoginPage --> LoginWithGoogle;
    HomePage --> AddNewTrip;
```

## Code fragments

### Manage user login and logout
This snippet is needed if you want to detect user 'movement' automatically and then choose which Screen show
`
final user = Provider.of<FirebaseUser>(context);

if (user == null) {
  return LoginScreen();
} else {
  return HomePage();
}
}
`

A similar snippet is used to get trips from firebase in real time

## Development details
### Dependencies
`firebase_core: ^0.4.`
`firebase_auth: ^0.15.4`
`cloud_firestore: ^0.13.3`
`flutter_spinkit: ^4.1.1`
`google_sign_in: ^4.1.1`
`provider: ^4.0.4`
`intl: ^0.16.1 // Library to format dates (and all locale related stuff)`
`material_design_icons_flutter: ^3.2.3895 // Some material icons`
`uuid: 2.0.4 // Generate an Unique ID ` 
`
### Problems and difficulties
- Learning how flutter works
- Implement firebase real-time events

### Reported Bugs
No bug is reported but you can report bugs using the dedicated section in GitLab

## References
[Flutter official doc](https://flutter.dev/docs)
