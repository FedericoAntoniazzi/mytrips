import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:intl/intl.dart';
import 'package:mytrips/animations/slide-right-route.dart';
import 'package:mytrips/database/storeHelper.dart';
import 'package:mytrips/models/trip.dart';
import 'package:mytrips/screens/trips/tripDetailsScreen.dart';

class TripTile extends StatelessWidget {
  final Trip trip;
  bool deletable;
  StoreHelper storeHelper;
  TripTile({this.trip, this.deletable, this.storeHelper});

  @override
  Widget build(BuildContext context) {
    void _showAlertDialog(
        String title, String text, Function positive, Function negative) {
      if (text.isEmpty) return;

      AlertDialog dialog = new AlertDialog(
        title: Text(title),
        content: Text(text),
        actions: <Widget>[
          FlatButton(
            onPressed: negative,
            child: Text("No"),
          ),
          FlatButton(
            onPressed: positive,
            child: Text("Yes"),
          ),
        ],
        elevation: 0,
        shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
      );
      showDialog(context: context, child: dialog);
    }

    return Container(
      padding: EdgeInsets.only(top: 6.0),
      child: Card(
        elevation: 0,
        shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
        margin: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 0.0),
        child: ListTile(
          leading: CircleAvatar(
            radius: 25.0,
            backgroundColor: Colors.blue,
          ),
          title: Text(trip.place),
          subtitle: Text("Departure date: " +
              DateFormat("dd-MM-yyyy").format(trip.departureDate)),
          trailing: deletable
              ? IconButton(
                  icon: Icon(Icons.delete_forever),
                  onPressed: () {
                    _showAlertDialog("Attention",
                        "You are going to delete this trip and you can't restore it later.\nAre you sure to continue?",
                        () {
                      storeHelper.deleteTrip(trip.id);
                      Navigator.pop(context);
                    }, () {
                      Navigator.pop(context);
                    });
                  },
                )
              : null,
          onTap: () {
            Navigator.push(
              context,
              SlideRightRoute(
                page: TripDetail(
                  trip: trip,
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}
