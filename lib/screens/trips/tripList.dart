import 'package:flutter/material.dart';
import 'package:mytrips/database/storeHelper.dart';
import 'package:mytrips/models/loading.dart';
import 'package:mytrips/models/trip.dart';
import 'package:mytrips/screens/trips/tripTile.dart';
import 'package:provider/provider.dart';

class TripList extends StatefulWidget {
  bool deletable;
  StoreHelper storeHelper;
  TripList({Key key, this.deletable, this.storeHelper}) : super(key: key);

  @override
  _TripListState createState() => _TripListState();
}

class _TripListState extends State<TripList> {
  @override
  Widget build(BuildContext context) {
    final trips = Provider.of<List<Trip>>(context);
    try {
      if (trips.length == 0) {
        return Center(
            child: Text(
          "You don't have any trip yet.\n Add one using the button below",
          textAlign: TextAlign.center,
        ));
      }
      return ListView.builder(
        itemCount: trips.length,
        itemBuilder: (context, index) {
          return TripTile(
              trip: trips[index],
              deletable: widget.deletable,
              storeHelper: widget.storeHelper);
        },
      );
    } catch (e) {
      // Sometimes there are problem with the trip download so there's a Loading animation
      return Loading();
    }
  }
}
