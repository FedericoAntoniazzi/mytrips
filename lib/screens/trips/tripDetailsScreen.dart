import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:mytrips/models/trip.dart';

class TripDetail extends StatelessWidget {
  Trip trip;

  TripDetail({Key key, this.trip}) : super(key: key);

  String dateToString(DateTime dt) {
    List<String> parts = dt.toString().split(' ')[0].split('-');
    return parts[2] + "/" + parts[1] + "/" + parts[0];
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("My Trips"),
        elevation: 0,
        centerTitle: true,
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.all(40.0),
          child: Column(
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Card(
                elevation: 0,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0)),
                child: Padding(
                    padding: EdgeInsets.all(10.0),
                    child: Text("Destination: " + trip.place)),
              ),
              Card(
                elevation: 0,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0)),
                child: Padding(
                    padding: EdgeInsets.all(10.0),
                    child: Text(
                        "Departure date: " + dateToString(trip.departureDate))),
              ),
              Card(
                elevation: 0,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0)),
                child: Padding(
                    padding: EdgeInsets.all(10.0),
                    child:
                        Text("Return date: " + dateToString(trip.returnDate))),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
