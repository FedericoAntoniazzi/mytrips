import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mytrips/database/storeHelper.dart';
import 'package:mytrips/models/loading.dart';
import 'package:mytrips/models/trip.dart';
import 'package:uuid/uuid.dart';

class AddTripScreen extends StatefulWidget {
  StoreHelper store;

  AddTripScreen({this.store});

  @override
  _AddTripScreenState createState() => _AddTripScreenState();
}

class _AddTripScreenState extends State<AddTripScreen> {
  final _formKey = GlobalKey<FormState>();
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  final _placeController = TextEditingController();
  bool loading = false;
  DateTime departureDate, returnDate;

  Future<Null> _selectDepartureDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: departureDate ?? DateTime.now(),
        firstDate: DateTime(2000, 1, 1),
        lastDate: returnDate ?? DateTime(2100));
    if (picked != null && picked != departureDate) {
      setState(() {
        departureDate = picked;
      });
    }
  }

  Future<Null> _selectReturnDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: departureDate ?? DateTime.now(),
        firstDate: departureDate ?? DateTime(2000, 1, 1),
        lastDate: DateTime(2100));
    if (picked != null && picked != returnDate) {
      setState(() {
        returnDate = picked;
      });
    }
  }

  String dateToString(DateTime dt) {
    List<String> parts = dt.toString().split(' ')[0].split('-');
    return parts[2] + "/" + parts[1] + "/" + parts[0];
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          title: Text("My Trips"),
          elevation: 0,
          centerTitle: true,
        ),
        body: loading
            ? Loading()
            : Padding(
                padding: EdgeInsets.all(40.0),
                child: Form(
                  key: _formKey,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      TextFormField(
                        decoration: InputDecoration(
                          labelText: "Place/City",
                          alignLabelWithHint: true,
                        ),
                        controller: _placeController,
                        validator: (value) {
                          if (value.trim().isEmpty) {
                            return "Place/City can't be empty";
                          }
                          return null;
                        },
                      ),
                      Divider(
                        height: 30.0,
                        color: Colors.transparent,
                      ),
                      Row(
                        children: <Widget>[
                          Text("Departure date: "),
                          FlatButton.icon(
                            onPressed: () => _selectDepartureDate(context),
                            icon: Icon(Icons.date_range),
                            label: departureDate == null
                                ? Text("Pick departure date")
                                : Text(dateToString(departureDate)),
                          )
                        ],
                      ),
                      Row(
                        children: <Widget>[
                          Text("Return date"),
                          FlatButton.icon(
                            onPressed: () => _selectReturnDate(context),
                            icon: Icon(Icons.date_range),
                            label: returnDate == null
                                ? Text("Pick Return date")
                                : Text(dateToString(returnDate)),
                          )
                        ],
                      ),
                      FlatButton(
                        child: Text("Add Trip"),
                        onPressed: () async {
                          String place = _placeController.text.trim();
                          if (departureDate == null || returnDate == null) {
                            _scaffoldKey.currentState.showSnackBar(SnackBar(
                                content: Text("Please, select a date")));
                            return;
                          }
                          if (returnDate.isBefore(departureDate)) {
                            _scaffoldKey.currentState.showSnackBar(SnackBar(
                                content: Text(
                                    "The return date cannot be before than departure date")));
                            return;
                          }
                          if (!_formKey.currentState.validate()) {
                            _scaffoldKey.currentState.showSnackBar(SnackBar(
                              content: Text("Place/City can't be empty"),
                            ));
                            return;
                          }

                          setState(() {
                            loading = true;
                          });
                          Trip t = new Trip(
                              id: Uuid().v1(),
                              place: place,
                              departureDate: departureDate,
                              returnDate: returnDate);
                          await widget.store.updateTrip(t);

                          setState(() {
                            loading = false;
                          });

                          Navigator.pop(context);
                        },
                      )
                    ],
                  ),
                ),
              ));
  }

  @override
  void dispose() {
    _placeController.dispose();
    super.dispose();
  }
}
