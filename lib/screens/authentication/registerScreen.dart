import 'package:flutter/material.dart';
import 'package:mytrips/models/forms/registerForm.dart';

class RegisterScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("My Trips"),
        elevation: 0,
        centerTitle: true,
      ),
      body: SingleChildScrollView(
        child: Padding(
            padding: EdgeInsets.all(40),
            child: Column(
              children: <Widget>[
                CircleAvatar(
                  child: Image.asset(
                    'images/app_icon.png',
                  ),
                  radius: 80.0,
                  backgroundColor: Colors.transparent,
                ),
                Divider(
                  height: 50.0,
                  color: Colors.transparent,
                ),
                RegisterForm(),
              ],
            )),
      ),
    );
  }
}
