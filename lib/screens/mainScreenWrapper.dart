import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:mytrips/screens/authentication/loginScreen.dart';
import 'package:mytrips/screens/homepage.dart';
import 'package:provider/provider.dart';

class MainScreenWrapper extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final user = Provider.of<FirebaseUser>(context);

    if (user == null) {
      return LoginScreen();
    } else {
      return HomePage();
    }
  }
}
