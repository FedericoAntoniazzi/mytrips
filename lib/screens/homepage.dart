import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:mytrips/animations/slide-right-route.dart';
import 'package:mytrips/database/authHelper.dart';
import 'package:mytrips/database/storeHelper.dart';
import 'package:mytrips/models/trip.dart';
import 'package:mytrips/screens/trips/addTripScreen.dart';
import 'package:mytrips/screens/trips/tripList.dart';
import 'package:provider/provider.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  bool deletable = false;

  @override
  Widget build(BuildContext context) {
    final user = Provider.of<FirebaseUser>(context);
    final _storeHelper = StoreHelper(currentUID: user.uid);

    void _showAlertDialog(
        String title, String text, Function positive, Function negative) {
      if (text.isEmpty) return;

      AlertDialog dialog = new AlertDialog(
        title: Text(title),
        content: Text(text),
        actions: <Widget>[
          FlatButton(
            onPressed: negative,
            child: Text("No"),
          ),
          FlatButton(
            onPressed: positive,
            child: Text("Yes"),
          ),
        ],
        elevation: 0,
        shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
      );
      showDialog(context: context, child: dialog);
    }

    return StreamProvider<List<Trip>>.value(
      value: _storeHelper.trips,
      child: Scaffold(
        appBar: AppBar(
          title: Text("My Trips"),
          elevation: 0,
          centerTitle: true,
          actions: <Widget>[
            IconButton(
              // Logout
              icon: Icon(Icons.exit_to_app),
              onPressed: () async => _showAlertDialog(
                  "Attention!",
                  "You are disconnecting your account. \n"
                      "Are you sure to continue?\n\n"
                      "You can re login with the same account and your trips will remain",
                  () {
                AuthHelper.instance.logOut();
                Navigator.pop(context);
              }, () {
                Navigator.pop(context);
              }),
            )
          ],
        ),
        body: TripList(
          deletable: deletable,
          storeHelper: _storeHelper,
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
        floatingActionButton: FloatingActionButton(
          elevation: 0,
          onPressed: () {
            Navigator.push(
                context,
                SlideRightRoute(
                    page: AddTripScreen(
                  store: _storeHelper,
                )));
          },
          child: Icon(Icons.add),
        ),
        bottomNavigationBar: BottomAppBar(
          shape: CircularNotchedRectangle(),
          elevation: 0,
          notchMargin: 7.0,
          child: Row(
            children: <Widget>[
              IconButton(
                icon: Icon(Icons.delete),
                onPressed: () {
                  setState(() => deletable = !deletable);
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
