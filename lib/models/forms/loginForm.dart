import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:mytrips/animations/slide-right-route.dart';
import 'package:mytrips/database/authHelper.dart';
import 'package:mytrips/models/loading.dart';
import 'package:mytrips/screens/authentication/registerScreen.dart';

class LoginForm extends StatefulWidget {
  @override
  _LoginFormState createState() => _LoginFormState();
}

class _LoginFormState extends State<LoginForm> {
  final _passwordController = TextEditingController();
  final _usernameController = TextEditingController();

  bool loading = false;

  @override
  Widget build(BuildContext context) {
    void showSnackbar(String msg) {
      Scaffold.of(context).showSnackBar(SnackBar(content: Text(msg)));
    }

    return loading
        ? Loading()
        : Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              TextFormField(
                decoration: InputDecoration(
                  labelText: "Username",
                  alignLabelWithHint: true,
                ),
                controller: _usernameController,
                validator: (val) {
                  if (val.trim().isEmpty) return "Username can't be empty";
                  return null;
                },
              ),
              Divider(
                height: 30.0,
                color: Colors.transparent,
              ),
              TextFormField(
                decoration: InputDecoration(
                  labelText: "Password",
                  alignLabelWithHint: true,
                ),
                obscureText: true,
                controller: _passwordController,
                validator: (val) {
                  if (val.trim().isEmpty) return "Password can't be empty";
                  return null;
                },
              ),
              Divider(
                height: 30.0,
                color: Colors.transparent,
              ),
              Column(
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      FlatButton(
                        onPressed: () {
                          Navigator.push(
                              context, SlideRightRoute(page: RegisterScreen()));
                        },
                        color: Colors.blue[400],
                        textColor: Colors.grey[100],
                        child: Text("Sign up"),
                      ),
                      SizedBox(
                        height: 1.0,
                        width: 10.0,
                      ),
                      FlatButton(
                        onPressed: () async {
                          // TODO: Texts cannot be null/empty
                          String username = _usernameController.text.trim();
                          String password = _passwordController.text.trim();

                          setState(() => loading = true);

                          try {
                            await AuthHelper.instance
                                .signInWithUsernameAndPassword(
                                    username, password);
                          } catch (e) {
                            showSnackbar(e.message);
                          }

                          setState(() => loading = false);
                        },
                        color: Colors.green[400],
                        textColor: Colors.grey[100],
                        child: Text("Login"),
                      ),
                    ],
                  ),
                  FlatButton.icon(
                    onPressed: () async {
                      setState(() => loading = true);
                      try {
                        await AuthHelper.instance.signInWithGoogle();
                        print("User signed in with google");
                        //Navigator.pushReplacement(context, SlideRightRoute(page: HomePage()));
                      } catch (e) {
                        print("User closed google menu");
                      }
                      // An exception is thrown due to the main Provider that checks user logins
                      setState(() => loading = false);
                    },
                    color: Colors.red,
                    textColor: Colors.grey[100],
                    icon: Icon(
                      MdiIcons.google,
                      color: Colors.grey[100],
                    ),
                    padding: EdgeInsets.symmetric(horizontal: 28.0),
                    label: Text("Login with Google"),
                  ),
                ],
              ),
            ],
          );
  }

  @override
  void dispose() {
    _passwordController.dispose();
    _usernameController.dispose();
    super.dispose();
  }
}
