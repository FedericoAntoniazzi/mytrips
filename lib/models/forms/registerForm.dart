import 'package:flutter/material.dart';
import 'package:mytrips/database/authHelper.dart';
import 'package:mytrips/models/loading.dart';

class RegisterForm extends StatefulWidget {
  @override
  _RegisterFormState createState() => _RegisterFormState();
}

class _RegisterFormState extends State<RegisterForm> {
  final _formKey = GlobalKey<FormState>();

  final _passwordController = TextEditingController();
  final _emailController = TextEditingController();
  // TODO: Save username into firebase
  // final _usernameController = TextEditingController();

  bool loading = false;

  @override
  Widget build(BuildContext context) {
    return loading
        ? Loading()
        : Form(
            key: _formKey,
            child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  /*
            TODO: Save username into firebase
            TextFormField(
              decoration: InputDecoration(
                labelText: "Username",
                alignLabelWithHint: true,
              ),
              controller: _usernameController,
              validator: (value) {
                if (value.trim().isEmpty) {
                  return "Enter a username";
                }
                return null;
              },
            ),
            Divider(
              height: 15.0,
              color: Colors.transparent,
            ),
             */
                  TextFormField(
                    decoration: InputDecoration(
                      labelText: "Email",
                      alignLabelWithHint: true,
                    ),
                    controller: _emailController,
                    validator: (value) {
                      if (value.trim().isEmpty) {
                        return "Enter an email";
                      }

                      RegExp isValidEmail = new RegExp(
                          r"""^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$""",
                          caseSensitive: false, multiLine: false);
                      if (!isValidEmail.hasMatch(value.trim())) {
                        return "Enter a valid email address";
                      }
                      return null;
                    },
                  ),
                  Divider(
                    height: 15.0,
                    color: Colors.transparent,
                  ),
                  TextFormField(
                    decoration: InputDecoration(
                      labelText: "Password",
                      alignLabelWithHint: true,
                      errorMaxLines: 3,
                    ),
                    obscureText: true,
                    controller: _passwordController,
                    validator: (value) {
                      if (value.trim().isEmpty) {
                        return "Enter a password";
                      }

                      RegExp isValidPassword = new RegExp(
                          r"""^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$""",
                          caseSensitive: false, multiLine: false);
                      if (!isValidPassword.hasMatch(value.trim())) {
                        return "Password must contain minimum 8 characters, at least one uppercase letter, one number and one special character";
                      }
                      return null;
                    },
                  ),
                  Divider(
                    height: 15.0,
                    color: Colors.transparent,
                  ),
                  TextFormField(
                    decoration: InputDecoration(
                      labelText: "Confirm password",
                      alignLabelWithHint: true,
                    ),
                    obscureText: true,
                    validator: (value) {
                      if (value != _passwordController.text) {
                        return "Password doesn't match";
                      }
                      return null;
                    },
                  ),
                  Divider(
                    height: 15.0,
                    color: Colors.transparent,
                  ),
                  FlatButton(
                    onPressed: () async {
                      if (!_formKey.currentState.validate()) {
                        return;
                      }
                      setState(() => loading = true);

                      String email = _emailController.text.trim();
                      String password = _passwordController.text.trim();
                      try {
                        dynamic result = await AuthHelper.instance
                            .createUserWithEmailAndPassword(email, password);

                        if (result != null) {
                          Navigator.pop(context);
                        }
                      } catch (e) {
                        print(e.toString());
                        showSnackbar(e.message);
                      }
                      setState(() => loading = false);
                    },
                    color: Colors.blue[400],
                    textColor: Colors.grey[100],
                    child: Text("Sign up"),
                  ),
                ]),
          );
  }

  void showSnackbar(String msg) {
    Scaffold.of(context).showSnackBar(SnackBar(content: Text(msg)));
  }

  @override
  void dispose() {
    _emailController.dispose();
    _passwordController.dispose();
    super.dispose();
  }
}
