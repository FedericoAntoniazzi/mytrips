class Trip {
  String id;
  String place;
  DateTime departureDate, returnDate;

  Trip({this.id, this.place, this.departureDate, this.returnDate});

  dynamic toMap() {
    return {
      'id': id,
      'place': place,
      'departureDate': departureDate.toString(),
      'returnDate': returnDate.toString()
    };
  }
}
