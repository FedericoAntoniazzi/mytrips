import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';

class AuthHelper {
  final FirebaseAuth _auth = FirebaseAuth.instance;

  // Singleton approach
  static final AuthHelper instance = AuthHelper._singleton();
  AuthHelper._singleton();

  // Keep listening user login and logout events
  // Return null if the user is NOT logged in
  // Return FirebaseUser if the user is logged in
  Stream<FirebaseUser> get user {
    return _auth.onAuthStateChanged;
  }

  Future logOut() async {
    await _auth.signOut();
  }

  Future currentUser() async {
    FirebaseUser user = await _auth.currentUser();
    return user;
  }

  Future signInWithGoogle() async {
    GoogleSignIn _gSignIn = GoogleSignIn();
    GoogleSignInAccount gUser = await _gSignIn.signIn();
    GoogleSignInAuthentication gAuth = await gUser.authentication;
    AuthCredential credential = GoogleAuthProvider.getCredential(
        idToken: gAuth.idToken, accessToken: gAuth.accessToken);

    AuthResult ar = await _auth.signInWithCredential(credential);
    return ar.user;
  }

  Future createUserWithEmailAndPassword(String email, String password) async {
    AuthResult ar = await _auth.createUserWithEmailAndPassword(
        email: email, password: password);
    return ar.user;
  }

  Future signInWithUsernameAndPassword(String email, String password) async {
    AuthResult ar = await _auth.signInWithEmailAndPassword(
        email: email, password: password);
    return ar.user;
  }
}
