import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:mytrips/models/trip.dart';

class StoreHelper {
  final String currentUID;
  final _store = Firestore.instance;

  StoreHelper({this.currentUID});

  Future updateTrip(Trip userTrip) async {
    return await _store
        .collection("userData/$currentUID/trips")
        .document("${userTrip.id}")
        .setData(userTrip.toMap());
  }

  Future deleteTrip(String tripID) async {
    await _store
        .collection("userData/$currentUID/trips")
        .document("$tripID")
        .delete();
  }

  List<Trip> _brewListFromSnapshot(QuerySnapshot snapshot) {
    return snapshot.documents.map((doc) {
      return Trip(
        id: doc.data['id'] ?? '',
        place: doc.data['place'] ?? '',
        departureDate: DateTime.parse(doc.data['departureDate']) ?? null,
        returnDate: DateTime.parse(doc.data['returnDate']) ?? null,
      );
    }).toList();
  }

  Stream<List<Trip>> get trips {
    return _store
        .collection("userData/$currentUID/trips")
        .snapshots()
        .map(_brewListFromSnapshot);
  }
}
