import 'package:flutter/material.dart';
import 'package:mytrips/themes/themes.dart';

class _ThemeManager extends InheritedWidget {
  final ThemeManagerState data;

  _ThemeManager({
    this.data,
    Key key,
    @required Widget child,
  }) : super(key: key, child: child);

  @override
  bool updateShouldNotify(_ThemeManager oldWidget) {
    return true;
  }
}

class ThemeManager extends StatefulWidget {
  final Widget child;
  final MyThemeKeys initialThemeKey;

  const ThemeManager({
    Key key,
    this.initialThemeKey,
    @required this.child,
  }) : super(key: key);

  @override
  ThemeManagerState createState() => new ThemeManagerState();

  static ThemeData of(BuildContext context) {
    _ThemeManager inherited =
        (context.dependOnInheritedWidgetOfExactType<_ThemeManager>());
    return inherited.data.theme;
  }

  static ThemeManagerState instanceOf(BuildContext context) {
    _ThemeManager inherited =
        (context.dependOnInheritedWidgetOfExactType<_ThemeManager>());
    return inherited.data;
  }
}

class ThemeManagerState extends State<ThemeManager> {
  ThemeData _theme;

  ThemeData get theme => _theme;

  @override
  void initState() {
    _theme = MyThemes.getThemeFromKey(widget.initialThemeKey);
    super.initState();
  }

  void changeTheme(MyThemeKeys themeKey) {
    setState(() {
      _theme = MyThemes.getThemeFromKey(themeKey);
    });
  }

  @override
  Widget build(BuildContext context) {
    return new _ThemeManager(
      data: this,
      child: widget.child,
    );
  }
}
