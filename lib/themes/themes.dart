import 'package:flutter/material.dart';

enum MyThemeKeys { LIGHT, DARK, DARKER }

class MyThemes {
  // TextBox with a green border to indicate that it has focus
  static final OutlineInputBorder _focusedBorder = OutlineInputBorder(
    borderSide: BorderSide(
      color: Colors.green,
      width: 1.75,
    ),
    borderRadius: BorderRadius.circular(10.0),
  );

  // TextBox with a grey border to indicate that it doesn't have focus
  static final OutlineInputBorder _normalBorder = OutlineInputBorder(
    borderSide: BorderSide(
      color: Colors.grey[500],
    ),
    borderRadius: BorderRadius.circular(10.0),
  );

  static final ButtonThemeData _buttonTheme = ButtonThemeData(
    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
  );

  // Light theme inspired by Google's play store
  static final ThemeData lightTheme = ThemeData(
    primaryColor: Colors.grey[100],
    brightness: Brightness.light,
    accentColor: Colors.green,
    scaffoldBackgroundColor: Colors.grey[200],
    inputDecorationTheme: InputDecorationTheme(
      focusColor: Colors.green,
      labelStyle: TextStyle(color: Colors.grey[500]),
      focusedBorder: _focusedBorder,
      border: _normalBorder,
      isDense: true,
    ),
    buttonTheme: _buttonTheme,
    bottomAppBarColor: Colors.grey[100],
  );

  // Normal dark theme
  static final ThemeData darkTheme = ThemeData(
    primaryColor: Colors.grey[800],
    accentColor: Colors.green[800],
    brightness: Brightness.dark,
    inputDecorationTheme: InputDecorationTheme(
      focusColor: Colors.green,
      labelStyle: TextStyle(color: Colors.grey[500]),
      focusedBorder: _focusedBorder,
      border: _normalBorder,
      isDense: true,
    ),
    buttonTheme: _buttonTheme,
    iconTheme: IconThemeData(
      color: Colors.grey[100],
    ),
    floatingActionButtonTheme: FloatingActionButtonThemeData(
      backgroundColor: Colors.green[800],
    ),
    bottomAppBarColor: Colors.grey[800],
  );

  // Dark theme for AMOLED displays
  static final ThemeData darkerTheme = ThemeData(
    primaryColor: Colors.black,
    brightness: Brightness.dark,
    accentColor: Colors.green[800],
    inputDecorationTheme: InputDecorationTheme(
      focusColor: Colors.green,
      labelStyle: TextStyle(color: Colors.grey[500]),
      focusedBorder: _focusedBorder,
      border: _normalBorder,
      isDense: true,
    ),
    bottomAppBarColor: Colors.black,
    cardColor: Colors.black,
  );

  static ThemeData getThemeFromKey(MyThemeKeys themeKey) {
    switch (themeKey) {
      case MyThemeKeys.LIGHT:
        return lightTheme;
      case MyThemeKeys.DARK:
        return darkTheme;
      case MyThemeKeys.DARKER:
        return darkerTheme;
      default:
        return lightTheme;
    }
  }
}
