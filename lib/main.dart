import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:mytrips/database/authHelper.dart';
import 'package:mytrips/screens/mainScreenWrapper.dart';
import 'package:mytrips/themes/theme-manager.dart';
import 'package:mytrips/themes/themes.dart';
import 'package:provider/provider.dart';

void main() => runApp(ThemeManager(
      initialThemeKey: MyThemeKeys.LIGHT,
      child: MyApp(),
    ));

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    return StreamProvider<FirebaseUser>.value(
        value: AuthHelper.instance.user,
        child: MaterialApp(
          title: "My Trips",
          theme: ThemeManager.of(context),
          home: MainScreenWrapper(),
        ));
  }
}
